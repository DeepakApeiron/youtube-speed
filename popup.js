document.addEventListener("click",(e) => {

    function sendMessage(tabs){
        if(e.target.textContent === '+'){
            browser.tabs.sendMessage(tabs[0].id,{
                command: 'increase'
            });            
        }
        else if(e.target.textContent === '-') {
            
            browser.tabs.sendMessage(tabs[0].id,{
                command: 'decrease'
            });            
        }
        else if(e.target.textContent === '0') {
            
            browser.tabs.sendMessage(tabs[0].id,{
                command: 'reset'
            });            
        }
    }
    browser.tabs.query({"active":true, "currentWindow":true}, sendMessage)
        
});